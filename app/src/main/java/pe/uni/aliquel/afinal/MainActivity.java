package pe.uni.aliquel.afinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<PreguntaClass> preguntas = new ArrayList<>();
    int contador = 0;
    Button button_true, button_false, button_sig, button_ant;
    ImageView imgView;
    TextView pregunta_text;
    RelativeLayout relativeLayout;
    Boolean is_true = true, is_false = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        button_true = findViewById(R.id.button_true);
        button_false = findViewById(R.id.button_false);
        button_sig = findViewById(R.id.siguiente);
        button_ant = findViewById(R.id.anterior);
        imgView = findViewById(R.id.img_trivia);
        pregunta_text = findViewById(R.id.pregunta);
        fillArrayPregunta();

        button_true.setOnClickListener(v->{
            relativeLayout = findViewById(R.id.relative_layout);
            if(preguntas.get(contador).resp == is_true ){
                Snackbar.make(relativeLayout, R.string.rpt_correcta, Snackbar.LENGTH_LONG).show();
            }else{
                Snackbar.make(relativeLayout, R.string.rpt_false, Snackbar.LENGTH_LONG).show();
            }
        });

        button_false.setOnClickListener(v->{
            relativeLayout = findViewById(R.id.relative_layout);
            if( preguntas.get(contador).resp == is_false){
                Snackbar.make(relativeLayout, R.string.rpt_correcta, Snackbar.LENGTH_LONG).show();
            }else{
                Snackbar.make(relativeLayout,R.string.rpt_false, Snackbar.LENGTH_LONG).show();
            }
        });

        button_sig.setOnClickListener(v->{
            if(contador+1<preguntas.size()){
                contador=contador+1;
            }else{
                contador=0;
            }
            imgView.setImageResource(preguntas.get(contador).img);
            pregunta_text.setText(getResources().getString(preguntas.get(contador).pregunta));

        });
        button_ant.setOnClickListener(v->{
            if(contador-1>=0){
                contador = contador - 1;
            }else{
                contador=preguntas.size()-1;
            }
            imgView.setImageResource(preguntas.get(contador).img);
            pregunta_text.setText(getResources().getString(preguntas.get(contador).pregunta));
        });
    }
    void fillArrayPregunta(){
        preguntas.add(new PreguntaClass(R.string.Pregunta1, R.drawable.carro, true));
        preguntas.add(new PreguntaClass(R.string.Pregunta2, R.drawable.tf, false));
        preguntas.add(new PreguntaClass(R.string.Pregunta3, R.drawable.uni, true));
        preguntas.add(new PreguntaClass(R.string.Pregunta4, R.drawable.ceviche, false));
        preguntas.add(new PreguntaClass(R.string.Pregunta5, R.drawable.lemur, false));
        preguntas.add(new PreguntaClass(R.string.Pregunta6, R.drawable.moto, false));
        preguntas.add(new PreguntaClass(R.string.Pregunta7, R.drawable.serpiente, true));
        preguntas.add(new PreguntaClass(R.string.Pregunta8, R.drawable.peru, true));
    }
}
//primer commit
