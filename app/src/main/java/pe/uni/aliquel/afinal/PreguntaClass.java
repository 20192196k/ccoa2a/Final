package pe.uni.aliquel.afinal;
//Segun el txt la imágen, texto y rpta se debe crear una clase como esta
public class PreguntaClass {
    Integer pregunta;
    Integer img;
    Boolean resp;

    public PreguntaClass(Integer pregunta, Integer img, Boolean resp) {
        this.pregunta = pregunta;
        this.img = img;
        this.resp = resp;
    }

}
